<?php

#require_once __DIR__ . '/../surplus-concert/functions.php';

// implement better polylang support to setup post id support
if (function_exists('pll_get_post')) {
    add_filter('theme_mod_about_page_id', function ($post_id) {
        return pll_get_post($post_id);
    });
}

add_action( 'wp_enqueue_scripts', 'hkoscon_brand2020_enqueue_styles' );
function hkoscon_brand2020_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

function hkoscon_brand2020_copyright_years() {
    $start = 2020;
    $end = (int) date('Y');
    return ($start != $end)
        ? $start . '-' . $end
        : $end;
}

/**
 * Footer site info.
 */
function surplus_concert_footer_site_info(){ 
    /** Load default theme options */
    $default_options  = surplus_concert_default_theme_options();
    $footer_copyright = get_theme_mod( 'footer_copyright', $default_options['footer_copyright'] );
    ?>
    <div class="site-info">
        <div class="container">
            <?php 
                if( ! empty( $footer_copyright ) )
                    echo '<span class="copyright">'. hkoscon_brand2020_copyright_years() . ' ' . wp_kses_post( $footer_copyright ) .'</span>';

                /* translators: 1: Theme name, 2: Base theme name. */
                printf( ' ' . esc_html__( '%1$s, based on %2$s.', 'hkoscon-brand2020' ),
                    '<a target="_blank" href="' . esc_url('https://gitlab.com/hkoscon/hkoscon-brand2020/') . '">HKOSCon Branding</a>',
                    '<a target="_blank" href="' . esc_url( 'https://wordpress.org/themes/surplus-concert/' ) .'">Surplus Concert</a>'
                );

                echo '<span class="sep"> | </span>';

                /* translators: %s: CMS name, i.e. WordPress. */
                printf( esc_html__( 'Powered by %s. ', 'surplus-concert' ), '<a href="'. esc_url( 'https://wordpress.org' ) .'">'. esc_html__( 'WordPress', 'surplus-concert' ) .'</a>' );

                if ( function_exists( 'the_privacy_policy_link' ) ) {
                    the_privacy_policy_link();
                }
            ?>
     
        </div><!-- .container -->    
    </div><!-- .site-info -->
<?php
}

/**
 * Overrides author information.
 */
function surplus_concert_author_meta() {
    // show nothing.
    return;
}
