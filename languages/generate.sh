#!/bin/bash

# Make sure the folder is correct
cd $(dirname $0)/..

THEME_NAME=$(basename $(pwd))

# generate files list
find -type f -name '*.php' > files.txt

# generate pot file
xgettext \
	--from-code=utf-8 \
	-d . \
	--files-from=files.txt \
	--keyword=esc_html__:1 \
	--keyword=__:1 -o \
	languages/$THEME_NAME.pot

# remove temp file list
rm files.txt

sed -i 's/CHARSET/UTF-8/' languages/$THEME_NAME.pot

echo "generated languages/$THEME_NAME.pot"
