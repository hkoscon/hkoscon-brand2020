#!/bin/bash

#
# This file compile and install .mo files
# to the proper wordpress folder.
#

# Make sure the folder is correct
cd $(dirname $0)

# Generates mo files from po files
for file in *.po; do
	msgfmt -o ${file/.po/.mo} $file
done

# Create langauges folder, if not exists
if [ ! -d ../../../languages/themes/ ]; then
	mkdir -p ../../../languages/themes/
fi

# Copy the language files to proper location
for file in *.mo; do
	echo "install $file"
	cp -pdf $file ../../../languages/themes/.
done
