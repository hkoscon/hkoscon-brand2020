# HKOSCon Branding 2020

![screenshot]

Wordpress theme for [HKOSCon branding site][website].
Child Theme of [Surplus Concert][surplus-concert].

Project of HKOSCon 2020.

[screenshot]: screenshot.png
[website]: https://info.hkoscon.org
[surplus-concert]: https://wordpress.org/themes/surplus-concert/
